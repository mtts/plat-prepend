# Git prepare-commit-msg hook

Extracts PLAT-1234 from current branch and prepends it to commit message.


## One-liner install

```bash
cd my-repo/.git/hooks
curl -O https://bitbucket.org/mtts/plat-prepend/raw/master/prepare-commit-msg && chmod +x prepare-commit-msg
```

...